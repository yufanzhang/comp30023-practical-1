#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

typedef struct node process_n;
typedef struct memory memory_n;

struct node {
    int arrival_time;
    char process_name[10];
    int time_required;
    int time_required_orgin;
    int memory;
    char state[10];
    process_n* next;
    process_n* pre;
};

typedef struct {
    process_n* head;
    process_n* foot;
} queue_n;

struct memory{
    int memory_state;
    int number;
    char process_name[10];
    memory_n* next;
};

typedef struct{
    memory_n* head;
    memory_n* foot;
} queue_memory;

//---------signature---------//
void add_data(queue_n* q, int arrival_time, char* process_name, int time_required, int memory, char* state);
void add_node(queue_n* q, process_n* new_node);
void print_q(queue_n* q);
void read_data(char* filename, queue_n* batch_list);
void pickup_input(queue_n* batch_list, queue_n* input_list, int simulated_time);
void pickup_input_task3(queue_n* batch_list, queue_n* input_list, int simulated_time, queue_memory* memory_list);
void move_node(queue_n* q, process_n* node);
void dele_node(queue_n* q, process_n* node);
process_n* search_shortest_process(queue_n* q);
void add_node_to_list(memory_n* new_node, queue_memory* list);
memory_n* create_memory_node(int i);
int count_memory(queue_memory* q);
int allocate_memory(queue_memory* q, process_n* process, int simulated_time);
int find_position(memory_n* current_node,queue_memory* q);
void free_memory(queue_memory* q, process_n* process);
int count_list(queue_n* q);
//---------signature end---------//

int main(int argc, char *argv[]) {

    int quantum;
    char line[100];
    char *filename = NULL;
    int simulated_time = 0;
    int remaining_time = 0;
    char *scheduler_algorithm = NULL;
    char *memory_strategy = NULL;

    //create batch list, input list and ready list
    queue_n* batch_list = (queue_n*)malloc(sizeof(queue_n));
    batch_list->head = batch_list->foot = NULL;
    queue_n* input_list = (queue_n*)malloc(sizeof(queue_n));
    input_list->head = input_list->foot = NULL;
    queue_n* ready_list = (queue_n*)malloc(sizeof(queue_n));
    ready_list->head = ready_list->foot = NULL;

    //read the input
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-f") == 0 && i + 1 < argc) {
            filename = argv[i + 1];
            read_data(filename, batch_list);
        } else if (strcmp(argv[i], "-s") == 0 && i + 1 < argc) {
            scheduler_algorithm = argv[i + 1];
        } else if (strcmp(argv[i], "-m") == 0 && i + 1 < argc) {
            memory_strategy = argv[i + 1];
        } else if (strcmp(argv[i], "-q") == 0 && i + 1 < argc) {
            quantum = atoi(argv[i + 1]);
        }
    }
    
    // create memory
    queue_memory* memory_list = (queue_memory*)malloc(sizeof(queue_memory));
    memory_list->foot = memory_list->head = NULL;
    // create node
    for(int i=0; i < 2048; i++){
        memory_n* new_node = create_memory_node(i);
        add_node_to_list(new_node,memory_list);
    }

    //=================================TASK 1=================================//
    if(strcmp(scheduler_algorithm,"SJF") == 0 && strcmp(memory_strategy,"infinite") == 0 ){
        // put all the process which arrive time <= simmulated time
        pickup_input(batch_list, input_list, simulated_time);
        double num = count_list(batch_list);
        process_n* processing = NULL;
        double turnaround_time = 0;
        double end = 0;
        double single = 0;
        double time_overhead = 0;
        double time_single = 0;
        double max_time_overhead = 0;
        while(input_list->head != NULL){
            // pick up the shortest by time_required
            processing = search_shortest_process(input_list);
            strcpy(processing->state, "RUNNING");
            printf("%d,%s,process_name=%s,remaining_time=%d \n",simulated_time,processing->state,processing->process_name,processing->time_required);

            remaining_time = processing->time_required;
            while(remaining_time > 0){
                simulated_time+=quantum;
                remaining_time-=quantum;
            }
            // calculate time
            end = simulated_time;
            single = end - processing->arrival_time;
            turnaround_time += single;
            time_single = single/processing->time_required;
            if(max_time_overhead<time_single){
                max_time_overhead = time_single;
            }
            time_overhead += time_single;

            strcpy(processing->state, "FINISHED");
            // move finished process from input list
            move_node(input_list, processing);
            // add all process except this cycle
            pickup_input(batch_list, input_list, simulated_time-quantum);
            int proc_remaining = count_list(input_list);
            printf("%d,%s,process_name=%s,proc_remaining=%d \n",simulated_time,processing->state,processing->process_name,proc_remaining);
            // add all process
            pickup_input(batch_list, input_list, simulated_time);
        }
        turnaround_time = ceil(turnaround_time/(num+1));
        time_overhead = round((time_overhead / (num+1)) *100)/100;
        max_time_overhead = round(max_time_overhead*100)/100;
        printf("Turnaround time %0.f\nTime overhead %.2f %.2f \nMakespan %d\n",turnaround_time,max_time_overhead,time_overhead,simulated_time);
    }
    //================================TASK 1 END================================//

    //==================================TASK 2==================================//
    else if(strcmp(scheduler_algorithm,"RR") == 0 && strcmp(memory_strategy,"infinite") == 0 ){
        double num = count_list(batch_list);
        pickup_input(batch_list,ready_list,simulated_time);
        process_n* current = ready_list->head;
        process_n* box = NULL;
        
        double turnaround_time = 0;
        double end = 0;
        double single = 0;
        double time_overhead = 0;
        double time_single = 0;
        double max_time_overhead = 0;

        while(ready_list->head != NULL || batch_list->head != NULL){
            //pickup_input_task3(batch_list, ready_list, simulated_time, memory_list);
            //print_q(ready_list);
            if(ready_list->head == NULL){
                current = NULL;
                goto finish;
            }
            if(current == NULL){
               current = ready_list->head;
            }
            int moved = 0;
            // if process is compeleted
            if(current->time_required <= 0){
                strcpy(current->state, "FINISHED");

                end = simulated_time;
                single = end - current->arrival_time;
                turnaround_time += single;
                time_single = single / current->time_required_orgin;
                if(max_time_overhead < time_single){
                    max_time_overhead = time_single;
                }
                time_overhead += time_single;
                //int proc_remaining = count_list(ready_list);
                
                process_n* temp = ready_list->head;
                int proc_remaining = 0;
                while(temp != NULL){
                    if(temp->arrival_time < simulated_time - quantum){
                        proc_remaining ++;
                    }
                    temp = temp->next;
                }
                proc_remaining = proc_remaining - 1;
                printf("%d,%s,process_name=%s,proc_remaining=%d\n",simulated_time,current->state,current->process_name,proc_remaining);
                if(current->next != NULL){
                    box = current->next;
                }else if(current == ready_list->foot){
                    box = ready_list->head;
                }
                dele_node(ready_list,current);
                current = box;
                moved = 1;
            }
            if(ready_list->foot != ready_list->head){
                // many nodes
                if(strcmp(current->state,"null") == 0){
                    // if there are many same nodes, just modify first one
                    strcpy(current->state, "RUNNING");
                    printf("%d,%s,process_name=%s,remaining_time=%d \n",simulated_time,current->state,current->process_name,current->time_required);
                    current->time_required -= quantum;
                    goto finish;
                }
                strcpy(current->state, "READY");
                if(moved != 1){
                    if(current->next != NULL){
                        current = current->next;
                    }else if(current == ready_list->foot){
                        current = ready_list->head;
                    }
                }
                if(strcmp(current->state,"null") == 0){
                    strcpy(current->state, "RUNNING");
                    printf("%d,%s,process_name=%s,remaining_time=%d \n",simulated_time,current->state,current->process_name,current->time_required);
                    current->time_required -= quantum;
                }else if(strcmp(current->state,"READY") == 0){
                    strcpy(current->state, "RUNNING");
                    printf("%d,%s,process_name=%s,remaining_time=%d \n",simulated_time,current->state,current->process_name,current->time_required);
                    current->time_required -= quantum;
                }
            }
            if(current == ready_list->head && current == ready_list->foot){
                // there are only one process
                if(strcmp(current->state,"RUNNING") != 0){
                    strcpy(current->state, "RUNNING");
                    printf("%d,%s,process_name=%s,remaining_time=%d \n",simulated_time,current->state,current->process_name,current->time_required);
                }
                current->time_required -= quantum;
            }
            finish:
            simulated_time += quantum;
            pickup_input(batch_list, ready_list, simulated_time);
        }
        max_time_overhead = round(max_time_overhead*100)/100;
        turnaround_time = ceil(turnaround_time/(num));
        time_overhead = round((time_overhead / num) *100)/100;
        printf("Turnaround time %0.f\nTime overhead %.2f %.2f \nMakespan %d\n",turnaround_time,max_time_overhead,time_overhead,simulated_time-quantum);

    }
    //==================================TASK 2 END==================================//

    //==================================TASK 3==================================//
    else if(strcmp(scheduler_algorithm,"RR") == 0 && strcmp(memory_strategy,"best-fit") == 0){
        
        // pick up the process who's arrive time is suitable
        double num = count_list(batch_list);
        pickup_input_task3(batch_list,ready_list,simulated_time,memory_list);
        process_n* current = ready_list->head;
        process_n* box = NULL;

        double turnaround_time = 0;
        double end = 0;
        double single = 0;
        double time_overhead = 0;
        double time_single = 0;
        double max_time_overhead = 0;

        // make sure no process in ready list and batch list
        while(ready_list->head != NULL || batch_list->head != NULL){
            start:
            while(ready_list->head == NULL && batch_list->head != NULL){
                
                simulated_time+=quantum;
                pickup_input_task3(batch_list,ready_list,simulated_time,memory_list);
                //urn_on = 1;
            }
            if(current == NULL && ready_list->head != NULL){
                current = ready_list->head;
            }

            // if process is compeleted
            if(current->time_required <= 0){
                strcpy(current->state, "FINISHED");
                end = simulated_time;
                single = end - current->arrival_time;
                turnaround_time += single;
                time_single = single / current->time_required_orgin;
                if(max_time_overhead < time_single){
                    max_time_overhead = time_single;
                }
                time_overhead += time_single;

                free_memory(memory_list,current);
                int proc_remaining = count_list(ready_list) - 1;
                printf("%d,%s,process_name=%s,proc_remaining=%d\n",simulated_time,current->state,current->process_name,proc_remaining);
                
                
                if (proc_remaining == 0){
                    box = NULL;
                }else{
                    if(current->next != NULL && current != ready_list->foot){
                        box = current->next;
                    }
                    else if(current == ready_list->foot){
                        box = ready_list->head;
                    }
                }

                dele_node(ready_list,current);
                current = box;
                
                if(current == NULL){
                    pickup_input_task3(batch_list,ready_list,simulated_time,memory_list);
                    if(ready_list->head != NULL && batch_list->head != NULL){
                        current = ready_list->head;
                    }
                    else{
                        goto finish_task3;
                    }
                }
            }
            if(ready_list->foot != ready_list->head){
                // many nodes
                if(strcmp(current->state,"null") == 0){
                    // if there are many same nodes, just modify first one
                    strcpy(current->state, "RUNNING");
                    printf("%d,%s,process_name=%s,remaining_time=%d \n",simulated_time,current->state,current->process_name,current->time_required);
                    current->time_required -= quantum;
                }
                strcpy(current->state, "READY");
                if(current->next != NULL){
                    current = current->next;
                }else if(current == ready_list->foot){
                    current = ready_list->head;
                }
                if(strcmp(current->state,"null") == 0){
                    strcpy(current->state, "RUNNING");
                    printf("%d,%s,process_name=%s,remaining_time=%d \n",simulated_time,current->state,current->process_name,current->time_required);
                    current->time_required -= quantum;
                }else if(strcmp(current->state,"READY") == 0){
                    strcpy(current->state, "RUNNING");
                    printf("%d,%s,process_name=%s,remaining_time=%d \n",simulated_time,current->state,current->process_name,current->time_required);
                    current->time_required -= quantum;
                }

            }
            if(current == ready_list->head && current == ready_list->foot){
                // there are only one process
                if(strcmp(current->state,"RUNNING") != 0){
                    strcpy(current->state, "RUNNING");
                    printf("%d,%s,process_name=%s,remaining_time=%d \n",simulated_time,current->state,current->process_name,current->time_required);
                }
                current->time_required -= quantum;
            }
            int before_num = 0;
            before_num = count_list(ready_list);
            int after_num = 0;

            finish_task3:
            
            pickup_input_task3(batch_list, ready_list, simulated_time, memory_list);
            
            after_num = count_list(ready_list);
            if(after_num!=before_num && ready_list->head!=NULL && batch_list->head != NULL){
                goto start;
            }
            
            simulated_time += quantum;
            
            if(current == NULL && ready_list->head != NULL){
                current = ready_list->head;
            }
        }
        
        max_time_overhead = round(max_time_overhead*100)/100;
        turnaround_time = ceil(turnaround_time/(num));
        time_overhead = round((time_overhead / num) *100)/100;
        printf("Turnaround time %0.f\nTime overhead %.2f %.2f \nMakespan %d\n",turnaround_time,max_time_overhead,time_overhead,simulated_time-quantum);

        
    }

    //=====================Task 3 SJF===================//
    if(strcmp(scheduler_algorithm,"SJF") == 0 && strcmp(memory_strategy,"best-fit") == 0){
        // put all the process which arrive time <= simmulated time
        pickup_input_task3(batch_list, input_list, simulated_time,memory_list);
        double num = count_list(batch_list);
        process_n* processing = NULL;
        double turnaround_time = 0;
        double end = 0;
        double single = 0;
        double time_overhead = 0;
        double time_single = 0;
        double max_time_overhead = 0;

        while(input_list->head != NULL){
            // pick up the shortest by time_required
            processing = search_shortest_process(input_list);
            strcpy(processing->state, "RUNNING");
            printf("%d,%s,process_name=%s,remaining_time=%d \n",simulated_time,processing->state,processing->process_name,processing->time_required);

            remaining_time = processing->time_required;
            while(remaining_time > 0){
                simulated_time+=quantum;
                pickup_input_task3(batch_list, input_list, simulated_time,memory_list);
                remaining_time-=quantum;
            }
            // calculate time
            end = simulated_time;
            single = end - processing->arrival_time;
            turnaround_time += single;
            time_single = single/processing->time_required;
            if(max_time_overhead<time_single){
                max_time_overhead = time_single;
            }
            time_overhead += time_single;

            strcpy(processing->state, "FINISHED");
            free_memory(memory_list,processing);

            // move finished process from input list
            move_node(input_list, processing);
            // add all process except this cycle

            int proc_remaining = count_list(input_list);
            printf("%d,%s,process_name=%s,proc_remaining=%d \n",simulated_time,processing->state,processing->process_name,proc_remaining);
            // add all process
            pickup_input_task3(batch_list, input_list, simulated_time,memory_list);
        }
        turnaround_time = ceil(turnaround_time/(num+1));
        time_overhead = round((time_overhead / (num+1)) *100)/100;
        max_time_overhead = round(max_time_overhead*100)/100;
        printf("Turnaround time %0.f\nTime overhead %.2f %.2f \nMakespan %d\n",turnaround_time,max_time_overhead,time_overhead,simulated_time);
    }
    return 0;
}
//=====================Task 3 SJF END===================//

//---------------functions---------------//
int allocate_memory(queue_memory* q, process_n* process, int simulated_time){
    int min = -1;
    memory_n* min_start = NULL;
    memory_n* min_end = NULL;
    memory_n* temp = q->head;

    while(temp->next != NULL && temp != NULL){

        // find the start of 0
        while(temp->memory_state != 0 && temp->next != NULL){
            temp = temp->next;
        }
        memory_n* temp_start = temp;

        // find the end of 0/
        int count_memory = 1;
        while(temp->memory_state != 1 && temp->next!=NULL){
            temp = temp->next;
            count_memory++;
        }
        memory_n* temp_end = temp;

        // check the memeory size
        if(count_memory < process->memory && min_end == NULL){
            return 0;
        }
        if (count_memory == process->memory){
            memory_n* temp_allocate = temp_start;
            for (int i = 0; i < process->memory; i++){
                temp_allocate -> memory_state = 1;
                strcpy(temp_allocate->process_name,process->process_name);
                temp_allocate = temp_allocate->next;
            }
            printf("%d,READY,process_name=%s,assigned_at=%d\n",simulated_time, process->process_name, temp_start->number);
            return 1;
        }else if(count_memory > process->memory){
            if(min == -1){
                min = count_memory;
                min_start = temp_start;
                min_end = temp_end;
            }else{
                // compare to get min
                if(min > count_memory){
                    min = count_memory;
                    min_start = temp_start;
                    min_end = temp_end;
                }
            }
        }
    }

    int position = find_position(min_start,q);
    int size = min_end->number - min_start->number + 1;
    if(min != -1){
        if (process->memory){
            for (size_t i = 0; i < process->memory; i++){
                min_start->memory_state = 1;
                strcpy(min_start->process_name,process->process_name);
                min_start = min_start->next;
            }
            printf("%d,READY,process_name=%s,assigned_at=%d\n",simulated_time, process->process_name, position);
            return 1;
        }
        return 0;
    }
    return 0;
}

// received node and then free it
void free_memory(queue_memory* q, process_n* process){
    memory_n* temp = q->head;
    // firstly, find the location where need free
    while(strcmp(temp->process_name,process->process_name) != 0 && temp->next != NULL){
        temp = temp->next;
    }

    while(strcmp(temp->process_name,process->process_name)==0 && temp->next != NULL){
        temp->memory_state = 0;
        strcpy(temp->process_name,"null");
        temp = temp->next;
    }
}
// received the list find the position
int find_position(memory_n* current_node,queue_memory* q){
    int position = 0;
    memory_n* temp = q->head;
    while(temp != current_node){
        temp = temp->next;
        position++;
    }
    return position;
}

// count how many memory
int count_memory(queue_memory* q){
    memory_n* temp = q->head;
    int num = 0;
    while(temp != NULL){
        if(temp->memory_state == 0){
            temp = temp->next;
            num ++;
        }
    }
    return num;
}

// add node to list
void add_node_to_list(memory_n* new_node, queue_memory* list) {
    if (list->head == NULL) {
        list->head = new_node;
        list->foot = new_node;
    } else {
        list->foot->next = new_node;
        list->foot = new_node;
    }
}

// create node
memory_n* create_memory_node(int i) {
    memory_n* node = (memory_n*)malloc(sizeof(memory_n));
    strcmp(node->process_name,"null");
    node->number = i;
    node->memory_state = 0;
    node->next = NULL;
    return node;
}

// delete node
// received list and node need to dispare, disconnect current node from current list
void dele_node(queue_n* q, process_n* node){
    // In the head
    if (node == q->head) {
        // Only one node in the list
        if (q->head == q->foot) {
            q->head = q->foot = NULL;
        } else {
            // More than one node in the list
            q->head = node->next;
            node->next->pre = NULL;
            node->next = NULL;
        }
    }
    // In the middle
    else {
        // In the foot
        if (node == q->foot) {
            node->pre->next = NULL;
            q->foot = node->pre;
            node->pre = NULL;
        } else {
            // In the middle
            node->pre->next = node->next;
            node->next->pre = node->pre;
            node->pre = node->next = NULL;
        }
    }
    free(node);
}

int count_list(queue_n* q){
    process_n* temp = q->head;
    int num = 0;
    while(temp != NULL){
        temp = temp->next;
        num ++;
    }
    return num;
}

// receive a list find the shortest.
process_n* search_shortest_process(queue_n* q){
    process_n* shortest = q->head;
    process_n* temp = q->head->next;
    while (temp != NULL) {
        if (temp->time_required < shortest->time_required) {
            shortest = temp;
        }
        else if (temp->time_required == shortest->time_required && temp->arrival_time < shortest->arrival_time){
            shortest = temp;
        } else if (temp->time_required == shortest->time_required && temp->arrival_time == shortest->arrival_time && (strcmp(temp->process_name,shortest->process_name)<0)) {
            shortest = temp;
        }
        temp = temp->next;
    }
    return shortest;
}
// receive two list, put the process which arrived time is <= simulated_time
void pickup_input_task3(queue_n* batch_list, queue_n* input_list, int simulated_time, queue_memory* memory_list){
    process_n* temp = batch_list->head;
    
    while (temp != NULL) {
        //find arrive time <=> simulated-time
        if(temp->arrival_time <= simulated_time){
            process_n* temp_next = temp->next;
            int sta = allocate_memory(memory_list,temp,simulated_time);

            if (sta==1){
                //1.delete node from current list
                move_node(batch_list, temp);
                //2.add node to aim list
                add_node(input_list, temp);
                //allocate_memory(memory_list,temp,simulated_time);
            }
            temp = temp_next;
        }else{
            temp = temp->next;
        }
    }
    
}

// receive two list, put the process which arrived time is <= simulated_time
void pickup_input(queue_n* batch_list, queue_n* input_list, int simulated_time){
    process_n* temp = batch_list->head;
    while (temp != NULL) {
        //find arrive time <=> simulated-time
        if(temp->arrival_time <= simulated_time){
            process_n* temp_next = temp->next;
            //1.delete node from current list
            move_node(batch_list, temp);
            //2.add node to aim list
            add_node(input_list, temp);
            //allocate_memory(memory_list,temp,simulated_time);
            temp = temp_next;
        }else{
            temp = temp->next;
        }
    }
    
}

// received list and node need to dispare, disconnect current node from current list
void move_node(queue_n* q, process_n* node){
    // In the head
    if (node == q->head) {
        // Only one node in the list
        if (q->head == q->foot) {
            q->head = q->foot = NULL;
        } else {
            // More than one node in the list
            q->head = node->next;
            node->next->pre = NULL;
            node->next = NULL;
        }
    }
    // In the middle
    else {
        // In the foot
        if (node == q->foot) {
            node->pre->next = NULL;
            q->foot = node->pre;
            node->pre = NULL;
        } else {
            // In the middle
            node->pre->next = node->next;
            node->next->pre = node->pre;
            node->pre = node->next = NULL;
        }
    }

}

// receive filename and linked list, then save all the input into list
void read_data(char* filename, queue_n* batch_list) {
    char line[100];
    FILE *fp;
    fp = fopen(filename, "r");
    if (fp == NULL) {
        printf("can't open file %s\n", filename);
        exit(1);
    }

    //save data into batch_list
    while (fgets(line, sizeof(line), fp)) {
        int arrival_time, time_required, memory_size;
        char process_name[10];
        sscanf(line, "%d %s %d %d", &arrival_time, process_name, &time_required, &memory_size);
        add_data(batch_list, arrival_time, process_name, time_required, memory_size, "null");
    }

    fclose(fp);
}

// receive data, save in node add then save into linked list
void add_data(queue_n* q, int arrival_time, char* process_name, int time_required, int memory, char* state) {
    process_n* new_node = (process_n*)malloc(sizeof(process_n));
    new_node->arrival_time = arrival_time;
    //strncpy(new_node->process_name, process_name, 9);
    strcpy(new_node->process_name, process_name);
    //new_node->process_name[9] = '\0';
    new_node->time_required = time_required;
    new_node->time_required_orgin = time_required;
    new_node->memory = memory;
    strcpy(new_node->state, state);
    new_node->next = new_node->pre = NULL;
    add_node(q, new_node);
}

// receive q linked list and new node, add node into linked list
void add_node(queue_n* q, process_n* new_node) {
    if (q->head == NULL){
        q->head = new_node;
        q->foot = new_node;
    }else{
        new_node->pre = q->foot;
        q->foot->next = new_node;
        q->foot = new_node;
    }
}

// receive linked list then print it
void print_q(queue_n* q) {
    process_n* temp = q->head;
    while (temp != NULL) {
        printf("arrival_time = %d, process_name = %s, time_required = %d, memory = %d, state = %s\n", temp->arrival_time, temp->process_name, temp->time_required, temp->memory, temp->state);
        temp = temp->next;
    }
}

//---------------functions end---------------//